#!/usr/bin/env bash

script_full_path=$(dirname "$0")

[ -d "${script_full_path}/venv/Scripts" ] && { (cd "${script_full_path}" && venv/Scripts/python.exe app.py "$@") }
[ -d "${script_full_path}/venv/bin" ] && { (cd "${script_full_path}" && venv/bin/python app.py "$@") }