# moodle-updated-bot

> This is just a small side project which I have no intentions of maintaining. Feel free to use or contribute.

## Description

Fetches Moodle courses to check if they have changed. Instead of visiting every course separately, this tools visits them in bulk.

This tool will not check if links have changed. It only checks the text on the course page. This is unfortunately very hard to implement, since Moodle changes the class names on every request. If the tool would compare the HTML (including `a`-tags and their `href`-parameters), it would trigger a change upon every check. This will maybe implemented some time in the future.

It also doesn't show what has changed.

## Usage 

Check every course defined in `config.json`:

```bash
$ python app.py
```

Check only specific courses (this will run much faster, since not all course pages have to be loaded):

```bash
$ python app.py Advanced
```

This checks only those courses starting with `Advanced`. If you just want to check a single course, enter the full course name (e.g. `"Advanced Programming"`). This is case insensitive.

You can also define multiple course prefixes:

```bash
$ python app.py Advanced Networks
```

This will check all courses starting with `Advanced` and all starting with `Networks`.

### Run from anywhere

You can use `app.sh` just as you would use `app.py` to run the app without having to activate the venv.

## Installation

Python3.7 required. Set up your venv just as always.

Don't forget to copy `config.json.example` to `config.json` and set your parameters. The course IDs can be extracted from the course URL (e.g. https://moodle.university.com/course/view.php?id=***9024***).

## Author

Florian Abensperg-Traun, [Website](https://floriantraun.at)

## License

![License](https://img.shields.io/:license-GNU%20GPLv3-blue.svg?style=flat-square)

This project is licensed under the GNU GPLv3.