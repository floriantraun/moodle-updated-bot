"""
Fetches Moodle courses to check if it has changed.
Instead of visiting every course separately, this tools visits them in bulk.

This tool will not check if links have changed. It only checks the text on the course page. This is unfortunately very
hard to implement, since Moodle changes the class names on every request. If the tool would compare the HTML (including
`a`-tags and their `href`-parameters), it would recognize a change upon every check.
This will maybe implemented some time in the future.

It also doesn't show what has changed.
"""

import json
import re
import sys
from hashlib import sha512
from os import path

import requests
from bs4 import BeautifulSoup

# Files.
CACHE_FILE = "hashes.json"
CONFIG_FILE = "config.json"

# Load config file.
with open(CONFIG_FILE) as file:
	conf = json.load(file)

# Moodle URLs.
LOGIN_URL = f"{conf['moodle_url']}/login/index.php"
COURSE_PREFIX = conf["moodle_url"] + "/course/view.php?id={}"

# Misc.
no_changes = True
DEBUG = conf["debug"]

if __name__ == '__main__':
	# Set headers and login payload.
	headers = {
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36",
		"Accept-Language": "en-US,en"
	}

	login_payload = {
		"username": conf["moodle_username"],
		"password": conf["moodle_password"]
	}

	# Exit the program if no courses have been defined.
	if not len(conf["course_ids"]):
		print(f"> No courses defined in {CONFIG_FILE}.")
		exit(1)

	# Assign course_ids defined in config.json to var course_ids.
	course_ids = conf["course_ids"]

	# Check if any parameters have been defined.
	if len(sys.argv) > 1:
		course_ids = {}  # Reset course_ids.

		for arg in sys.argv:  # Loop through arguments.
			for course in conf["course_ids"]:  # Loop through conf["course_ids"]
				if course.lower().startswith(arg.lower()):
					course_ids[course] = conf["course_ids"][course]  # Append the course.

	# Exit the program if no courses matching have been found.
	if not len(course_ids):
		print(f"> No courses matching the parameters defined in {CONFIG_FILE}.")
		exit(1)

	# Start session, retrieve login page.
	session = requests.Session()

	# Retrieve `logintoken` form field.
	soup = BeautifulSoup(session.get(LOGIN_URL).text, features="html.parser")
	login_payload["logintoken"] = soup.find("input", {"name": "logintoken"}).get(
		"value")  # Add `logintoken` to form payload.

	# Login to Moodle.
	login_request = session.post(LOGIN_URL, data=login_payload)

	# Check if login was successful.
	if login_request.text.find("loginerrormessage") > 0:
		print("Invalid credentials.")
		exit(1)

	# Loop through every course.
	for course in course_ids:
		gen_dict_hash = lambda x: sha512(json.dumps(x).encode("utf-8")).hexdigest()  # Create hashes from dict.

		# bs4 Object.
		soup = BeautifulSoup(session.get(COURSE_PREFIX.format(course_ids[course])).text, features="html.parser")

		# Parse page.
		course_page_content = soup.find("div", {"id": "page-content"}).get_text()  # Only use `div#page-content`
		__activity_head = soup.find("div", {
			"class": "activityhead"}).get_text()  # Remove `div.activityhead` (content changes every minute)
		course_page_content = re.sub(__activity_head, '', course_page_content)
		course_page_content = re.sub("\s+", '', course_page_content)  # Remove all whitespace

		# Hash variables.
		hash = sha512(course_page_content.encode("utf-8")).hexdigest()  # Create hash from page.

		cached_hashes = {}
		file_content_hash = gen_dict_hash(cached_hashes)  # Create hash from cached_hashes.

		# Write the page content to a file inside the `pages/` folder.
		if DEBUG:
			with open(f"pages/{course}.html", "wb+") as f:
				f.write(course_page_content.encode("utf-8"))

		# Create cache file if not exists.
		if not path.exists(CACHE_FILE):
			print(f"> {CACHE_FILE} does not exist. Creating.")
			with open(CACHE_FILE, "w+") as file:
				json.dump(cached_hashes, file, indent=4)
		else:
			# Load cache file.
			with open(CACHE_FILE, "r") as file:
				cached_hashes = json.load(file)
				file_content_hash = gen_dict_hash(cached_hashes)

		# Check if course has been cached previously.
		if not course in cached_hashes:
			no_changes = False
			print(f"'{course}' retrieved for the first time. Caching hash.")
			cached_hashes[course] = hash

		# Compare page hash with cached hash.
		if not hash == cached_hashes[course]:
			no_changes = False
			cached_hashes[course] = hash
			print(
				f"[!!] '{course}' has been updated! Access this course here: {COURSE_PREFIX.format(course_ids[course])}")

		# Write file.
		if not gen_dict_hash(cached_hashes) == file_content_hash:
			if DEBUG:
				print(f"> Updating {CACHE_FILE}")
			with open(CACHE_FILE, "w+") as file:
				json.dump(cached_hashes, file, indent=4)

	# Print info if nothing has changed.
	if no_changes:
		print("> No changes detected.")

	print("\nFinished.")
